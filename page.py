from dataclasses import dataclass


@dataclass
class Page:
    path: str
    title: str
    content: str
