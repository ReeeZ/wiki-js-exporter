from typing import List
from graph_ql_client import GraphQLClient
from page import Page
import json_parser
import os
import errno


class Exporter():
    def __init__(self, url: str, apiToken: str):
        self._client = GraphQLClient(url)
        self._client.inject_token(f"Bearer {apiToken}")

    def export(self):
        pageIds = self._get_page_ids()
        total = len(pageIds)
        current = 1
        for pageId in pageIds:
            print(f"Downloading Page {current}/{total}")
            page = self._get_page(pageId)
            Exporter._apply_con_workaround(page)
            self._write_page(page)
            current += 1

    def _get_page_ids(self) -> List[int]:
        request = '''
        query {
          pages {
            list{
              id
            }
          }
        }
        '''

        response = self._client.execute(request)
        return json_parser.parse_get_ids_response(response)

    def _get_page(self, id: int) -> Page:
        get_page_content_request = (
            "query {"
            "  pages {"
            f"   single(id: {id}) {{"
            "      path"
            "      title"
            "      content"
            "    }"
            "  }"
            "}")

        response = self._client.execute(get_page_content_request)
        return json_parser.parse_get_page_response(response)

    @staticmethod
    def _create_intermediate_dirs(filepath: str):
        if not os.path.exists(os.path.dirname(filepath)):
            try:
                os.makedirs(os.path.dirname(filepath))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

    @staticmethod
    def _write_page(page: Page):
        targetFile = f"export/{page.path}/{page.title}.md"
        Exporter._create_intermediate_dirs(targetFile)
        with open(targetFile, "w") as f:
            f.write(page.content)

    @staticmethod
    def _apply_con_workaround(page: Page):
        '''Con is not a valid name for a file or folder on a windows system'''
        loweredPath = page.path.lower()

        if (loweredPath.startswith("con/")):
            loweredPath = page.path.lower().replace("con/", "con-game/", 1)

        page.path = loweredPath.replace("/con/", "/con-game/")
        page.title = page.title.lower().replace("con", "con-game")
