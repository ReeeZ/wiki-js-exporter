# wiki js exporter

## Why

Small project for the personal purpose of exporting all pages of my wiki.js instance, because I want to take my server offline.

## GraphQl Client

The graph_ql_client was taken from [prisma-labs/python-graphql-client](https://github.com/prisma-labs/python-graphql-client) and slightly modified by me. Because I hd to disable the ssl certificate check.

I put the original License information in the `graph_ql_client.py` file. Which should be sufficent as I understand it.

## Disclaimer

This a kind of works-for-me-software. 

Exporting images does not work. I only noticed this in hindsight after my server went offline 🙃

## How to use

Edit the `api_token` variable in `export.py` to your api token. Then run

`python export.py`

Files will be exported to a subfolder called `export`

## Why the con workaround

I created a game called `con` with a friend. (Yeah, not a good idea in hindsight) We had some documentation about it in our wiki.js instance.
